# AirOS Enable DHCP

A small tool written in Rust to configure an AirOS device to provide a DHCP
server even when it's not possible via the web interface.

## Download

There are no versioned releases but you can download the latest successful build:

* [Windows binary](https://gitlab.com/serviushack/airos-enable-dhcp/-/jobs/artifacts/master/raw/airos-enable-dhcp.exe?job=windows)
* [Linux binary](https://gitlab.com/serviushack/airos-enable-dhcp/-/jobs/artifacts/master/raw/airos-enable-dhcp?job=linux)

## Usage

```
AirOS Enable DHCP 
Configure an AirOS device to provide a DHCP server even when it's not possible via the web interface.

USAGE:
    airos-enable-dhcp [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -b, --url <URL>                    The base URL under which the device is reachable
    -u, --username <USERNAME>          The username to authenticate with
    -p, --password <PASSWORD>          The password to authenticate with
        --dhcp-start <IP>              The first address of the DHCP range
        --dhcp-end <IP>                The last address of the DHCP range
        --dhcp-netmask <NETMASK>       The netmask to announce via DHCP
        --dhcp-lease-time <SECONDS>    The duration (in seconds) a DHCP lease is valid
        --dhcp-dns-proxy <STATE>       Whether the device should proxy DNS requests [possible values: enabled, disabled]
        --dhcp-dns-1 <IP>              The first DNS server to announce via DHCP
        --dhcp-dns-2 <IP>              The second DNS server to announce via DHCP
```

The tool will also ask interactively for missing parameters.
