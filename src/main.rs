use regex::Regex;
use reqwest::blocking::{multipart, Client};
use std::time::{SystemTime, UNIX_EPOCH};

mod cli;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let user_input = cli::read_user_input()?;

    // Accept invalid certs as the device by default only offers a self-signed certificate.
    let client = Client::builder()
        .cookie_store(true)
        .danger_accept_invalid_certs(true)
        .build()?;

    {
        let response = client
            .get(format!("{}/login.cgi", user_input.host))
            .send()?;
        if !response.status().is_success() {
            return Err("No 200 for GET /login.cgi".into());
        }
    }

    {
        let form = multipart::Form::new()
            .text("uri", "/index.cgi")
            .text("username", user_input.username)
            .text("password", user_input.password);

        let response = client
            .post(format!("{}/login.cgi", user_input.host))
            .multipart(form)
            .send()?;
        if !response.status().is_success() {
            return Err("No 200 for POST /login.cgi".into());
        }

        if !response.url().path().ends_with("/index.cgi") {
            return Err("Not redirected to index page, assuming failed.".into());
        }
    }

    let configuration = {
        let response = client
            .get(format!("{}/getcfg.sh?.", user_input.host))
            .send()?;
        if !response.status().is_success() {
            return Err("No 200 for GET /getcfg.sh?.".into());
        }
        response.text()?
    };

    let new_config = configuration
        .lines()
        .filter(|line| !line.starts_with("dhcpd."))
        .chain(user_input.dhcp_configuration.lines())
        .collect::<Vec<&str>>()
        .join("\n");

    let token = {
        let response = client
            .get(format!("{}/network.cgi", user_input.host))
            .send()?;
        if !response.status().is_success() {
            return Err("No 200 for GET /network.cgi".into());
        }
        let text = response.text()?;
        match Regex::new(r#"name="token" +value="([a-z0-9]+)""#)?.captures(&text) {
            Some(captures) => captures.get(1).unwrap().as_str().to_string(),
            None => return Err("No token found on network.cgi".into()),
        }
    };

    {
        let form = multipart::Form::new()
            .text("uri", "/network.cgi")
            .text("token", token)
            .text("network_data", new_config);

        let response = client
            .post(format!("{}/network.cgi", user_input.host))
            .multipart(form)
            .send()?;
        if !response.status().is_success() {
            return Err("No 200 for POST /network.cgi".into());
        }
    }

    {
        let response = client
            .get(format!("{}/apply.cgi", user_input.host))
            .query(&[
                ("testmode", ""),
                (
                    "_",
                    &SystemTime::now()
                        .duration_since(UNIX_EPOCH)?
                        .as_secs()
                        .to_string(),
                ),
            ])
            .send()?;
        if !response.status().is_success() {
            return Err("No 200 for GET /apply.cgi".into());
        }
    }

    Ok(())
}
