use clap::{App, Arg};
use dialoguer::{Confirm, Input};
use std::net::Ipv4Addr;
use url::Url;

pub struct UserInput {
    pub host: String,
    pub username: String,
    pub password: String,
    pub dhcp_configuration: String,
}

pub fn read_user_input() -> Result<UserInput, Box<dyn std::error::Error>> {
    let matches = App::new("AirOS Enable DHCP")
        .about("Configure an AirOS device to provide a DHCP server even when it's not possible via the web interface.")
        .arg(Arg::with_name("Base URL")
             .display_order(1)
             .short("b")
             .long("url")
             .takes_value(true)
             .value_name("URL")
             .help("The base URL under which the device is reachable")
             .validator(|url| validate_url(&url))
             )
        .arg(Arg::with_name("Username")
             .display_order(2)
             .short("u")
             .long("username")
             .takes_value(true)
             .value_name("USERNAME")
             .help("The username to authenticate with")
             )
        .arg(Arg::with_name("Password")
             .display_order(3)
             .short("p")
             .long("password")
             .takes_value(true)
             .value_name("PASSWORD")
             .help("The password to authenticate with")
             )
        .arg(Arg::with_name("DHCP start")
             .display_order(4)
             .long("dhcp-start")
             .takes_value(true)
             .value_name("IP")
             .help("The first address of the DHCP range")
             .validator(|ip| validate_ip(&ip))
             )
        .arg(Arg::with_name("DHCP end")
             .display_order(5)
             .long("dhcp-end")
             .takes_value(true)
             .value_name("IP")
             .help("The last address of the DHCP range")
             .validator(|ip| validate_ip(&ip))
             )
        .arg(Arg::with_name("DHCP netmask")
             .display_order(6)
             .long("dhcp-netmask")
             .takes_value(true)
             .value_name("NETMASK")
             .help("The netmask to announce via DHCP")
             .validator(|ip| validate_ip(&ip))
             )
        .arg(Arg::with_name("DHCP lease time")
             .display_order(7)
             .long("dhcp-lease-time")
             .takes_value(true)
             .value_name("SECONDS")
             .help("The duration (in seconds) a DHCP lease is valid")
             .validator(|number| validate_number(&number))
             )
        .arg(Arg::with_name("DHCP DNS Proxy")
             .display_order(8)
             .long("dhcp-dns-proxy")
             .takes_value(true)
             .value_name("STATE")
             .possible_values(ENABLED_DISABLED_VALUES)
             .help("Whether the device should proxy DNS requests")
             )
        .arg(Arg::with_name("DHCP first DNS server")
             .display_order(9)
             .long("dhcp-dns-1")
             .takes_value(true)
             .value_name("IP")
             .help("The first DNS server to announce via DHCP")
             .validator(|ip| validate_ip_or_empty(&ip))
             )
        .arg(Arg::with_name("DHCP second DNS server")
             .display_order(10)
             .long("dhcp-dns-2")
             .takes_value(true)
             .value_name("IP")
             .help("The second DNS server to announce via DHCP")
             .validator(|ip| validate_ip_or_empty(&ip))
             )
        .get_matches();

    // Get values provided either via parameters or ask user.
    let host = get_user_input_with_default(
        &matches,
        "Base URL",
        "https://192.168.1.20".to_string(),
        &validate_url,
    )?;
    let username =
        get_user_input_with_default(&matches, "Username", "ubnt".to_string(), &no_validate)?;
    let password =
        get_user_input_with_default(&matches, "Password", "ubnt".to_string(), &no_validate)?;
    let dhcp_start = get_user_input(&matches, "DHCP start", &validate_ip)?;
    let dhcp_end = get_user_input(&matches, "DHCP end", &validate_ip)?;
    let dhcp_netmask = get_user_input_with_default(
        &matches,
        "DHCP netmask",
        "255.255.255.0".to_string(),
        &validate_ip,
    )?;
    let dhcp_lease_time = get_user_input_with_default(
        &matches,
        "DHCP lease time",
        "600".to_string(),
        &validate_number,
    )?;
    let dhcp_dns_proxy = get_user_input_with_default(
        &matches,
        "DHCP DNS Proxy",
        "disabled".to_string(),
        &validate_enabled,
    )?;
    let dhcp_dns_1 = get_user_input_with_default(
        &matches,
        "DHCP first DNS server",
        "".to_string(),
        &validate_ip_or_empty,
    )?;
    let dhcp_dns_2 = get_user_input_with_default(
        &matches,
        "DHCP second DNS server",
        "".to_string(),
        &validate_ip_or_empty,
    )?;

    let dhcp_configuration = format!(
        "dhcpd.1.start={}
dhcpd.1.netmask={}
dhcpd.1.lease_time={}
dhcpd.1.end={}
dhcpd.1.dnsproxy={}
dhcpd.1.devname=br0
dhcpd.1.dns.2.server={}
dhcpd.1.dns.1.server={}
dhcpd.1.status=enabled
dhcpd.status=enabled",
        dhcp_start, dhcp_netmask, dhcp_lease_time, dhcp_end, dhcp_dns_proxy, dhcp_dns_2, dhcp_dns_1
    );

    println!(
        "\nThe following configuration will be written:\n\n{}\n",
        dhcp_configuration
    );
    if !Confirm::new()
        .with_prompt("Do you want to write this configuration to the device?")
        .interact()?
    {
        Err("Aborted because of missing user confirmation".into())
    } else {
        Ok(UserInput {
            host,
            username,
            password,
            dhcp_configuration,
        })
    }
}

fn get_user_input_with_default(
    matches: &clap::ArgMatches,
    label: &str,
    default: String,
    validator: &dyn Fn(&String) -> Result<(), String>,
) -> Result<String, std::io::Error> {
    match matches.value_of(label) {
        Some(host) => Ok(host.to_string()),
        None => Input::<String>::new()
            .with_prompt(label)
            .default(default)
            .validate_with(validator)
            .interact_text(),
    }
}

fn get_user_input(
    matches: &clap::ArgMatches,
    label: &str,
    validator: &dyn Fn(&String) -> Result<(), String>,
) -> Result<String, std::io::Error> {
    match matches.value_of(label) {
        Some(host) => Ok(host.to_string()),
        None => Input::<String>::new()
            .with_prompt(label)
            .validate_with(validator)
            .interact_text(),
    }
}

#[allow(clippy::ptr_arg)]
fn no_validate(_value: &String) -> Result<(), String> {
    Ok(())
}

#[allow(clippy::ptr_arg)]
fn validate_number(value: &String) -> Result<(), String> {
    value.parse::<i64>().map(|_| ()).map_err(|e| e.to_string())
}

static ENABLED_DISABLED_VALUES: &[&str] = &["enabled", "disabled"];

#[allow(clippy::ptr_arg)]
fn validate_enabled(value: &String) -> Result<(), String> {
    if ENABLED_DISABLED_VALUES.contains(&value.as_str()) {
        Ok(())
    } else {
        Err("Must be 'enabled' or 'disabled'".to_string())
    }
}

#[allow(clippy::ptr_arg)]
fn validate_url(value: &String) -> Result<(), String> {
    Url::parse(value).map(|_| ()).map_err(|e| e.to_string())
}

#[allow(clippy::ptr_arg)]
fn validate_ip(value: &String) -> Result<(), String> {
    value
        .parse::<Ipv4Addr>()
        .map(|_| ())
        .map_err(|e| e.to_string())
}

#[allow(clippy::ptr_arg)]
fn validate_ip_or_empty(value: &String) -> Result<(), String> {
    if value.is_empty() {
        Ok(())
    } else {
        value
            .parse::<Ipv4Addr>()
            .map(|_| ())
            .map_err(|e| e.to_string())
    }
}
